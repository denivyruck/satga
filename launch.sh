#!/bin/bash

scenarios=( 'uf20-01.cnf' 'uf50-01.cnf' 'uf75-01.cnf' 'uf100-01.cnf' 'uf250-01.cnf' )

for s in "${scenarios[@]}"
do
	pypy satga.py -ps 50 -ngener 2000 -niter 5 -cp 0.9 -mp 0.1 -gap 1.0 -s tournament 2 -c uniform -f $s &
done
