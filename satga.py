import random
import argparse
import sys
import os
import re

from timeit import default_timer
from numpy import std, mean
from copy import deepcopy
from math import sqrt

class Population(object):
	def __init__(self, pop_size, cross_prob, mut_prob, values_list, nr_vars):
		self.ppl = []
		self.pop_size = pop_size

		self.cross_prob = cross_prob
		self.mut_prob = mut_prob

		self.values_list = values_list
		self.nr_vars = nr_vars

	def generate_population(self):
		#values_list is equal for everyone
		#The only stochastic component is the vars
		for i in range(self.pop_size):
			var_list = [Var() for i in range(self.nr_vars)]

			guy = Sat(self.values_list, var_list)
			self.ppl.append(guy)

	def evaluate_fitness(self):
		for guy in self.ppl:
			guy.fitness()

	def evaluate_crossover(self):
		selected = []
		next_pop = []
		idx = -1

		if selection == 'roulette':
			for i in range(int(gap*self.pop_size)):
				if i%2 == 0:
					ind = self.roulette(-1)
					idx = ind
				else:
					ind = self.roulette(idx)
				selected.append(self.ppl[ind])

			#Copy the rest
			for i in range(len(selected), self.pop_size):
				selected.append(self.ppl[i])
			
			#Now we cross each 2
			for i in range(0, self.pop_size-1, 2):
				ind1 = selected[i]
				ind2 = selected[i+1]
				
				new1, new2 = ind1.crossover(ind2, self.cross_prob)
				next_pop.append(new1)
				next_pop.append(new2)
			self.ppl = deepcopy(next_pop)
		elif selection == 'tournament':
			for i in range(int(gap*self.pop_size)):
				if i%2 == 0:
					ind = self.tournament(-1)
					idx = ind
				else:
					ind = self.tournament(idx)
				selected.append(self.ppl[ind])

			#Copy the rest
			for i in range(len(selected), self.pop_size):
				selected.append(self.ppl[i])

			#Now we fornicate
			for i in range(0, pop_size-1, 2):
				ind1 = selected[i]
				ind2 = selected[i+1]
				
				new1, new2 = ind1.crossover(ind2, self.cross_prob)
				next_pop.append(new1)
				next_pop.append(new2)
			self.ppl = deepcopy(next_pop)

	#Idx is the guy I want to avoid
	def tournament(self, idx):
		dudes = []

		for j in range(2):
			rand = random.randrange(0, self.pop_size)
			while rand in dudes and rand != idx:
				rand = random.randrange(0, self.pop_size)
			dudes.append(rand)
		
		maxx = 0
		chosen = -1
		for i in range(2):
			if self.ppl[dudes[i]].fit > maxx:
				maxx = self.ppl[dudes[i]].fit
				chosen = dudes[i]
		return chosen
	
	#Idx is the guy to avoid
	def roulette(self, idx):
		sum_fit = sum([self.ppl[i].fit for i in range(self.pop_size)])

		value = random.random()*sum_fit
		for j in range(self.pop_size):
			if j != idx:
				value -= self.ppl[j].fit
				if value <= 0:
					return j
		return self.pop_size-1

	def get_diversity(self):
		acc = 0
		cont = 0

		for i in range(self.pop_size):
			dude = self.ppl[i]
			for j in range(i+1, self.pop_size):
				mate = self.ppl[j]
				acc += sum([1 for k in range(len(mate.var_list)) if dude.var_list[k].value != mate.var_list[k].value])
				cont += 1
		return acc/cont

	def evaluate_mutation(self):
		for guy in self.ppl:
			guy.bitflip_mutation(self.mut_prob)
			
	def get_best_ind(self):
		self.evaluate_fitness()
		return max(self.ppl, key=lambda ind : ind.fit)

	def print_clauses(self):
		for guy in self.ppl:
			out = '('

			for clause in guy.clauses_list:
				if out != '(':
					out += ' ^ ('

				for expr in clause.expr:
					if expr.is_true():
						out += 'T'
					else:
						out += 'F'
				out += ')'
			print(out)

	def print_population(self):
		self.evaluate_fitness()
		
		print('Population: ')
		for guy in self.ppl:
			out = ''
			for var in guy.var_list:
				if var.value == False:
					out += 'F'
				else:
					out += 'T'
			print('Individual: %s \t Fitness: %d' % (out, guy.fit))

class Clause(object):
	def __init__(self, expr):
		#List of expressions
		self.expr = expr

	def is_true(self, var_list):
		for e in self.expr:
			#FNC - Only 1 needs to be true
			if e.is_true(var_list):
				return True
		return False

class Expression(object):
	def __init__(self, var, flag):
		self.var = var
		self.flag = flag

	def is_true(self, var_list):
		#Flag means it's a ~Var
		if self.flag:
			return not var_list[self.var].value
		return var_list[self.var].value

class Var(object):
	def __init__(self):
		values = [True, False]
		#self.value = values[random.randrange(0, 2)]
		self.value = [True, False][random.randrange(0, 2)]

	def negate(self):
		self.value = not self.value

class Sat(object):
	def __init__(self, values_list, var_list):
		#List containing -8 0 1 etc
		self.values_list = values_list
		self.var_list = var_list
		self.clauses_list = self.build_clauses()

		self.fit = 0
		
		if crossover == 'onecut':
			self.crossover = self.onecut_crossover
		if crossover == 'uniform':
			self.crossover = self.uniform_crossover

	def build_clauses(self):
		clauses_list = []
		for line in self.values_list:
			expr = []
			for value in line:
				var = self.var_list[abs(value)-1]
				if value < 0:
					#Means it has a ~
					expression = Expression(abs(value)-1, True)
				else:
					expression = Expression(abs(value)-1, False)
				expr.append(expression)
			clause = Clause(expr)
			clauses_list.append(clause)
		return clauses_list

	def fitness(self):
		#self.build_clauses()

		self.fit = 0
		for clause in self.clauses_list:
			if clause.is_true(self.var_list):
				self.fit += 1
		
	#Bin
	def bitflip_mutation(self, prob):
		for var in self.var_list:
			rand = random.random()
			if rand <= prob:
				var.negate()

	#Real/bin
	def onecut_crossover(self, ind, cross_prob):
		prob = random.random()
		
		#Can't change the originals as they can crossover again later
		new1 = deepcopy(self)
		new2 = deepcopy(ind)
		if prob < cross_prob:
			#Area of cut
			rand = random.randrange(0, len(self.var_list))
			for i in range(rand):
				new1.var_list[i], new2.var_list[i] = deepcopy(new2.var_list[i]), deepcopy(new1.var_list[i])
		return new1, new2
		
	#Real/bin
	def uniform_crossover(self, ind, cross_prob):
		#Generates a random binary mask. If it's 1, exchange info. Guarantees diversity
		new1 = deepcopy(self)
		new2 = deepcopy(ind)
		
		if random.random() < cross_prob:
			mask = [random.randrange(0,2) for i in range(len(self.var_list))]
			for i in range(len(mask)):
				if mask[i] == 1:
					new1.var_list[i], new2.var_list[i] = new2.var_list[i], new1.var_list[i]
		return new1, new2

def parse_file(filename):
	f = open(filename, 'r')
			
	data = []
	for line in f:
		if line[0] != 'c' and line[0] != '0' and line[0] != '%':
			data.append(re.findall(r'[+-]?\d+', line))

	for i in range(len(data)):
		for j in range(len(data[i])):
			data[i][j] = int(data[i][j])
	
	nr_vars = int(data[0][0])
	clauses = int(data[0][1])
	del data[0]

	#Build clauses list. Each clause has 3 logic expressions
	values_list = []
	values_list = [data[i][:3] for i in range(clauses)]
	
	return values_list, nr_vars

def main():
	#Saves the average firness of each generation
	avg_fitness = [0 for i in range(n_gener)]
	avg_best = [0 for i in range(n_gener)]
	avg_diversity = [0 for i in range(n_gener)]
	time = 0.0

	values_list, nr_vars = parse_file(filename)

	print('Starting with file %s' % filename)	

	for k in range(n_iter):
		start = default_timer()
		pop = Population(pop_size, cross_prob, mut_prob, values_list, nr_vars)
		pop.generate_population()
		pop.evaluate_fitness()
		#pop.print_population()
		for i in range(n_gener):
			best = deepcopy(pop.get_best_ind())
			avg_best[i] += best.fit
			avg_diversity[i] += pop.get_diversity()
			
			avg = sum([guy.fit for guy in pop.ppl])/len(pop.ppl)
			avg_fitness[i] += avg
			pop.evaluate_fitness()
			pop.evaluate_crossover()
			pop.evaluate_mutation()

			pop.ppl[random.randrange(len(pop.ppl))] = best
		time += (default_timer() - start)
		#pop.print_population()
	time /= n_iter
	
	for i in range(n_gener):
		avg_fitness[i] /= n_iter
		avg_best[i] /= n_iter

	sd = std(avg_fitness)

	#out = open(fname+'.dat', 'w')
	out = open(filename.split('.')[0]+'.dat', 'w')	
	for i in range(n_gener):
		out.write("%d %.6lf %.6lf %.6lf %.6lf\n" % (i, avg_fitness[i], avg_best[i], avg_diversity[i], sd))
	out.write('Std: %.6lf \t Melhor ind: %d \t Tempo: %.4lf' % (sd, pop.get_best_ind().fit, time))

	out.close()	

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Do stuff')
	parser.add_argument('-ps', dest='ps', action='store', type=int, nargs=1, help='Pop size: N')
	parser.add_argument('-ngener', dest='ng', action='store', type=int, nargs=1, help='Nr generations: N')
	parser.add_argument('-niter', dest='nt', action='store', type=int, nargs=1, help='Nr iterations: N')
	parser.add_argument('-cp', dest='cp', action='store', type=float, nargs=1, help='Cross probability: [0.0..1.0]')
	parser.add_argument('-mp', dest='mp', action='store', type=float, nargs=1, help='Mutation probability: [0.0..1.0]')
	parser.add_argument('-gap', dest='gap', action='store', type=float, nargs=1, help='Generation gap = [0.0..1.0]')
	parser.add_argument('-s', '--sel_proc', dest='selection', action='store', nargs='+', help="Selection function: roulette/tournament")
	parser.add_argument('-c', '--cross_proc', dest='crossover', action='store', nargs=1, help="Cross-over function: onecut/uniform/blend/arithmetic")
	parser.add_argument('-f', dest='filename', action='store', nargs=1, help='Filename: string')
	args = parser.parse_args() 
	
	pop_size = args.ps[0]
	n_gener = args.ng[0]
	n_iter = args.nt[0]
	cross_prob = args.cp[0]
	mut_prob = args.mp[0]
	gap = args.gap[0]
	crossover = args.crossover[0]
	selection = args.selection[0]
	filename = args.filename[0]

	main()
