#!/usr/bin/gnuplot

reset

#set terminal pngcairo size 1920,1080 enhanced font 'Verdana,9'
set terminal pngcairo size 800,600 enhanced font 'Verdana,9'

set yrange [0:]
set autoscale
set xlabel 'Gerações'
set ylabel 'Função fitness'
set key outside
set key top left

#set for [i=1:5] linetype i dt i
#Draw only bottom and left borders with black (-1)
set border 3 back ls -1 

set tics nomirror
set style line 12 lc rgb '#808080' lt 0 lw 1

set style line 1 lt 1 lc rgb "red" lw 1
set style line 2 lt 2 lc rgb "blue" lw 1

set style line 5 lc rgb  '#ffff01' pt 1  ps 1 lt 1 lw 2
set style line 16 lc rgb '#5e9c36' pt 2  ps 1 lt 1 lw 2 
set style line 4 lc rgb  '#8b1a0e' pt 4  ps 1 lt 1 lw 2 
set style line 1 lc rgb  '#ff0000' pt 5  ps 1 lt 1 lw 2 
set style line 6 lc rgb  '#8b1a0e' pt 6  ps 1 lt 1 lw 2
set style line 3 lc rgb  '#00ff00' pt 7  ps 1 lt 1 lw 2 
set style line 8 lc rgb  '#1972e6' pt 8  ps 1 lt 1 lw 2
set style line 2 lc rgb '#0000ff' pt 15 ps 1 lt 1 lw 2

set grid back ls 12

set output 'uf20-01output_results.png'
plot 'uf20-01.dat' u 1:2 title 'Avg população' w l ls 1, \
	 'uf20-01.dat' u 1:3 title 'Melhor indivíduo' w l ls 2

set ylabel 'Diversidade'
set output 'uf20-01output_diversity.png'
plot 'uf20-01.dat' u 1:4 title 'Diversidade' w l ls 4 # dt 3
	 
